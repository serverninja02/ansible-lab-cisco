# Ansible-Lab-Cisco Repo

## Directory Setup in this Repo

 - **playbooks** - Ansible Playbooks
 - **inventory** - contains static inventories of hosts and groups
     * **group\_vars** - vars specific to groups referenced in inventory
     * **host\_vars** - vars specific to hosts referenced in inventory
 - **roles** - all reusable Ansible roles placed here

**Configs:**

 - **ansible.cfg** - configurations for ansible defined here specific to this repo
 - **requirements.txt** - To be used with Python Pip to install Ansible


## Requirements

 - Minimum Python 2.7 or newer installed
     * Python Pip installed
 - OpenSSH Client (best if doing this from a linux/unix/OSX  workstation, but it can be done in Windows)

 - Use Python Pip to install Ansible. In the root of this repository, run the following command to install the correct version of Ansible to be used with these playbooks.

```
pip install -r requirements.txt
```

# WMCUG Presentation / Demo - GNS3 Lab

 - [PowerPoint Slides](docs/PowerPoint-WMCUG-03_28_2018.pptx) (Clone this repo and see pptx file in docs folder)
 - [GNS3 Lab Setup](docs/GNS3_LAB.md)


# Learning Resources for Ansible

 - Ansible's Getting Started Tutorial - https://www.ansible.com/resources/get-started
 - (docs) Getting Started with Ansible - http://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html

# Recommended Books

 - Ansible Up and Running - By Lorin Hochstein, Rene Moser (O'Reilly Media)

# License (for the work in this repo)

Copyright [2018] [John W. Reed II]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
