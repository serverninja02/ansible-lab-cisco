# GNS3 Lab for this repo

## Connect these interfaces to a "Cloud" object... 
... which can be linked to a virtual network interface in either VMWare Fusion or VirtualBox

In my demonstration, I used vmnet1, which was connected to the "Private Network" in VMWare Fusion and could be attached to a secondary interface to linux VM running this Ansible code

Router  | Management IP | Management Interface
------------- | ------------- | -------------
gro  | 172.16.223.10/24 | fa0/0
hol  | 172.16.223.11/24 | fa0/0
dtw  | 172.16.223.12/24 | fa0/0

## Connect these interfaces to a switch or a "cloud" object, which can be linked to a linux VM in GNS3. 
In my demonstration, I acutally used cloud objects and attached them to a "Private Network" as I did above and changed the icons so they looked like PCs and Servers. You could go whater route makes sense for your setup.

Router  | Branch Office Network | Management Interface
------------- | ------------- | -------------
gro  | 10.1.0.1/16 | gi1/0
hol  | 10.2.0.1/16 | gi1/0
dtw  | 10.3.0.1/16 | gi1/0

## Connect these interfaces from router to router

Source Router  | Destination Router 
------------- | -------------
gro (192.168.253.1/30, gi4/0) | dtw (192.168.253.2/30, gi4/0)
gro (192.168.253.5/30, gi6/0) | hol (192.168.253.6/30, gi6/0)
hol (192.168.253.9/30, gi5/0) | dtw (192.168.253.10/30, gi5/0)

# Steps

## Initial configuration

Because we have no way of using Ansible with the GNS3 emulated serial console, we had to apply some configs firsthand:

 - [GRO Router Config](gro.txt)
 - [HOL Router Config](hol.txt)
 - [DTW Router Config](dtw.txt)

## Next, run the ansible playbooks as follows:
**NOTE: Make sure you run these commands from the root of this repo**

Apply login security banner:
```
ansible-playbook -vv playbooks/banner.yml
```

Apply configurations on all interfaces:
```
ansible-playbook -vv playbooks/router-interfaces.yml
```

Configure OSPF Routing:
```
ansible-playbook -vv playbooks/router-routing.yml
```

## Testing
You should be able to ping from a linux PC in GRO to another linux PC in DTW successfully

# Diagram / Layout in GNS3

![Diagram|50%](GNS3-Image.png "GNS3 Setup")


